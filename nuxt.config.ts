// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      title: "Titn Landing Page",
      meta: [
        { name: "description", content: "Description of TITN Landing Page." },
        { hid: "og:title", name: "og:title", content: "TITN Landing Page" },
      ],
      htmlAttrs: {
        lang: "en",
      },
      charset: "utf-8",
      viewport: "viewport-fit=cover, width=device-width, initial-scale=1",
    },
  },
  css: ["~/assets/css/main.css"],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  components: [
    {
      path: "~/components/atoms",
      pathPrefix: false,
    },
    {
      path: "~/components/molecules",
      pathPrefix: false,
    },
    {
      path: "~/components/organisms",
      pathPrefix: false,
    },
    {
      path: "~/components/templates",
      pathPrefix: false,
    },
  ],
});
